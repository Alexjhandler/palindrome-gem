require "ahandler_palindrome/version"

module AhandlerPalindrome

  def palindrome?
    processed_content == processed_content.reverse
  end

  private

  def processed_content
    self.to_s.scan(/[a-z]|\d/i).join.downcase
  end

end


class String
  include AhandlerPalindrome
end

class Integer
  include AhandlerPalindrome
end
