require "test_helper"

class AhandlerPalindromeTest < Minitest::Test

  def test_that_it_has_a_version_number
    refute_nil ::AhandlerPalindrome::VERSION
  end

  def test_non_palindrome
    refute "apple".palindrome?
  end

  def test_literal_palindrome
    assert "racecar".palindrome?
  end

  def test_mixed_case_palindrome
    assert "RaceCar".palindrome?
  end

  def test_palindrome_with_punctuation
    assert "Madam, I'm Adam".palindrome?
  end

  def test_int_non_palindrome
    refute 1234.palindrome?
  end

  def test_int_palindrome
    assert 12321.palindrome?
  end

end
